# systemd

// note: stdout of service is syslog


// add a service unit

#cp name.service /etc/systemd/system

#sudo systemctl start name.service

// edit name.service

#sudo gedit /etc/systemd/system

#sudo systemctl daemon-reload


// enable at boot

#sudo systemctl enable name.service


// view status 

#systemctl status name.service


// view log runtime

#journalctl -u name.service -f

or

#tail -f /var/log/syslog


